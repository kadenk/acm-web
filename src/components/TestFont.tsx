import { TestFontWeight, TestFontWeightItalic } from './TestFontWeight'

export interface FontTestProps {
    text: string
}

export default function TestFont({ text }: FontTestProps) {
    return (
        <>
            <div className="grid grid-cols-3">
                <div className="text-blue title">Title</div>
                <div className="text-blue sans">Sans</div>
                <div className="text-blue serif">Serif</div>
            </div>
            <div className="grid grid-cols-6">
                <TestFontWeight text={text} font="HK Grotesk" />
                <TestFontWeightItalic text={text} font="HK Grotesk" />
                <TestFontWeight text={text} font="Noto Sans" />
                <TestFontWeightItalic text={text} font="Noto Sans" />
                <TestFontWeight text={text} font="Roboto Slab" />
                <TestFontWeightItalic text={text} font="Roboto Slab" />
            </div>
        </>
    )
}
