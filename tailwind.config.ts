import type { Config } from 'tailwindcss'

export default {
    content: {
        relative: true,
        files: ['./src/**/*.{ts,tsx}'],
    },
    theme: {
        extend: {
            colors: {
                blue: 'rgb(0, 30, 105)',
                gold: 'rgb(248, 180, 53)',

                grey: 'rgb(167, 169, 172)',
                orange: 'rgb(226, 151, 52)',
                lightblue: 'rgb(0, 183, 250)',
                darkblue: 'rgb(17, 30, 53)',
            },
        },
    },
    plugins: [],
} satisfies Config
